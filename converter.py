import cv2
import numpy as np
from Tkinter import Tk
from tkinter.filedialog import askopenfile
from network import load
from preprocessing import histogram, otsu_threshold, binarization, erode, project, squeeze
from segment import PrintedCharacterSegmentator, PrintedConnectedCharacterSegmentator

data = {
    0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e',
    5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j',
    10: 'k', 11: 'l', 12: 'm', 13: 'n', 14: 'o',
    15: 'p', 16: 'q', 17: 'r', 18: 's', 19: 't',
    20: 'u', 21: 'v', 22: 'w', 23: 'x', 24: 'y',
    25: 'z'
}


class PrintedCharacterConverter:

    def __init__(self, segmentator, net):
        self.segmentator = segmentator
        self.network = net

    def transform(self):
        text = ''
        for word in self.segmentator.get_segmented_word():
            for ch in word:
                pr_y, pr_x = project(ch)
                st_y, end_y = squeeze(pr_y)
                st_x, end_x = squeeze(pr_x)
                _char = ch[st_y: end_y, st_x:end_x]
                _char = cv2.resize(_char, (28, 28))
                _char = np.reshape(_char, (784, 1))
                text += data[np.argmax(self.network.feed_forward(_char))]
                print text
                cv2.imshow("char", np.reshape(_char, (28, 28)))
                cv2.waitKey()
            text += ' '
        return text


def do_the_job():
    Tk().withdraw()
    op = askopenfile()
    if op.name[-4:] == ".png":
        img = cv2.imread(op.name, cv2.IMREAD_GRAYSCALE)
        histo = histogram(img)
        thresh = otsu_threshold(histo)
        im_bw = binarization(img, thresh)
        cv2.imshow("ed", im_bw)
        cv2.waitKey()
        return PrintedConnectedCharacterSegmentator(im_bw, (28, 28))
    return None


if __name__ == "__main__":
    it_end = 'n'
    Tk().withdraw()
    open_file = askopenfile()
    net = load(open_file.name)
    while it_end != 'y':
        seg = do_the_job()
        if seg is not None:
            conv = PrintedCharacterConverter(seg, net)
            print conv.transform()
        it_end = raw_input("End>[y/n]:")



