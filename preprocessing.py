import cv2
import numpy as np
import queue


neighbours = [[[-1, 0, 1, 0], [0, 1, 0, -1]], [[-1, -1, 0, 1, 1, 1, 0, -1], [0, 1, 1, 1, 0, -1, -1, -1]]]


def binarization(img, level):
    dest = np.zeros(img.shape, dtype=np.uint8)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if img[i, j] > level:
                dest[i, j] = 255
            else:
                dest[i, j] = 0
    return dest


def grey_level_histo(img):
    histo = np.zeros((256, ), 0)
    for line in img:
        for col in line:
            histo[col] += 1
    return histo


def valleys_peaks(histo, delta):
    valleys = []
    peaks = []
    _from = 0
    while histo[_from] == 0:
        _from += 1
    _to = histo.shape[0] - 1
    while histo[_to] == 0:
        _to -= 1
    _max = -1
    _min = 100000
    valleys.append(_from)
    look_for_peak = 1
    min_index = 0
    max_index = 0
    for i in range(_from + 1, _to):
        if _min > histo[i]:
            _min = histo[i]
            min_index = i

        if _max < histo[i]:
            _max = histo[i]
            max_index = i

        if look_for_peak == 1:
            if histo[i] < _max - delta:
                peaks.append(max_index)
                look_for_peak = 0
                _min = histo[i]
        elif look_for_peak == 0:
            if histo[i] > _min + delta:
                valleys.append(min_index)
                look_for_peak = 1
                _max = histo[i]
    if look_for_peak == 1:
        peaks.append(_to)
    elif look_for_peak == 0:
        valleys.append(_to)
    return valleys, peaks


def dilate(img, no_of_neigh=0, is_object_point=lambda x: True if x == 0 else False):
    ngh = neighbours[no_of_neigh]
    dest = np.copy(img)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if is_object_point(img[i, j]):
                for k in range((no_of_neigh + 1)*4):
                    if 0 <= ngh[0][k] + i < img.shape[0] and 0 <= ngh[1][k] + j < img.shape[1]:
                        dest[ngh[0][k] + i, ngh[1][k] + j] = 0
    return dest


def erode(img, no_of_neigh=0, is_object_point=lambda x: True if x < 255 else False):
    ngh = neighbours[no_of_neigh]
    dest = np.copy(img)
    for i in range(1, img.shape[0] - 1):
        for j in range(1, img.shape[1] - 1):
                for k in range((no_of_neigh + 1)*4):
                    if not is_object_point(img[ngh[0][k] + i, ngh[1][k] + j]):
                        dest[i, j] = 255

    return dest


def histogram(img, bins=256):
    if bins > 256:
        bins = 256
    if bins < 0:
        bins = 0
    bin_size = 256 / bins
    _histogram = np.zeros((bins, ), dtype=np.uint32)
    for line in img:
        for col in line:
            _histogram[col / bin_size] += 1
    return _histogram


def probability_histogram(histo):
    _prob_histogram = np.array(histo.shape, dtype=np.float32)
    total = float(np.sum(histo, axis=0))
    for i in range(histo.shape[0]):
        _prob_histogram[i] = float(histo[i]) / total
    return total, _prob_histogram


def otsu_threshold(histo):
    total = float(np.sum(histo, axis=0))
    wa = 0.0
    sum_a = 0.0
    _max = 0.0
    level = 0
    _total = np.sum(histo * np.array([i for i in range(256)], dtype=np.int32))
    for i in range(0, histo.shape[0]):
        wa += float(histo[i])
        wb = total - wa
        if wa == 0.0 or wb == 0.0:
            continue
        sum_a += i * histo[i]
        sum_b = _total - sum_a
        inter_class = wa * wb * ((sum_a / wa) - (sum_b / wb)) * ((sum_a / wa) - (sum_b / wb))
        if inter_class > _max:
            _max = inter_class
            level = i
    return level


def display_histogram(_histogram, height=480):
    _max = -1
    for val in _histogram:
        if _max < val:
            _max = val
    img = np.zeros((height, _histogram.shape[0]), dtype=np.uint8)
    for j in range(img.shape[1]):
        for i in range(int((float(_histogram[j]) / float(_max)) * float(height))):
            img[height - i - 1, j] = 255
    return img


def project(img, is_object_point=lambda x: True if x < 255 else False):
    histo_v = np.zeros(img.shape[0], dtype=np.uint32)
    histo_h = np.zeros(img.shape[1], dtype=np.uint32)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if is_object_point(img[i, j]):
                histo_v[i] += 1
                histo_h[j] += 1

    return histo_v, histo_h


def histogram_smoothing(_histogram, kernel_size=5):
    kernel = [i for i in range(-(kernel_size / 2), kernel_size/2 + 1)]
    dest = np.zeros((_histogram.shape[0], ), dtype=np.uint32)
    for i in range(_histogram.shape[0]):
        nos = 0
        val = 0
        for v in kernel:
            if 0 <= i + v < _histogram.shape[0]:
                val += float(_histogram[i + v])
                nos += 1.
        dest[i] = val / nos
    return dest


def get_geo(img, is_object=lambda x: True if x == 0 else False):
    _area = 0
    center_x = 0
    center_y = 0
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if is_object(img[i, j]):
                _area += 1
                center_x += j
                center_y += i

    return _area, center_x / _area, center_y / _area


def find_skew(img, is_object=lambda x: True if x == 0 else False):

    area, center_x, center_y = get_geo(img)
    x_sum = 0
    y_sum = 0
    xy_sum = 0
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if is_object(img[i, j]):
                x_sum += (j - center_x) * (j - center_x)
                y_sum += (i - center_y) * (i - center_y)
                xy_sum += (i - center_y) * (j - center_x)
    tang = 2.0 * float(xy_sum) / (float(x_sum - y_sum))
    return np.arctan(tang)


def squeeze(histo):
    _from = 0
    while histo[_from] == 0:
        _from += 1
    _to = histo.shape[0] - 1
    while histo[_to] == 0:
        _to -= 1
    if _from > 0:
        _from -= 1
    if _to < histo.shape[0] - 1:
        _to += 1
    return _from, _to


def extract_heights(img, is_object=lambda x: True if x == 0 else False):
    height = img.shape[0] - 1
    func = np.zeros((img.shape[1], ), dtype=np.int32)
    for j in range(img.shape[1]):
        for i in range(img.shape[0]):
            if is_object(img[i, j]):
                func[j] = height - i
                break
    return func


def extract_mins(img, is_object=lambda x: True if x == 0 else False):
    height = img.shape[0] - 1
    func = np.zeros((img.shape[1],), dtype=np.int32)
    for j in range(img.shape[1]):
        for i in reversed(range(img.shape[0])):
            if is_object(img[i, j]):
                func[j] = height - i
                break
    return func


def rotate_by_angle(img, angle, center_x, center_y, background=255):
    cos_a = np.cos(angle)
    sin_a = np.sin(angle)
    rotation_matrix = [[cos_a, -sin_a], [sin_a, cos_a]]
    _img = np.ones(img.shape, dtype=np.uint8) * background
    for i in range(_img.shape[0]):
        for j in range(_img.shape[1]):
            new_x = int(rotation_matrix[0][0] * float(j - center_x) + rotation_matrix[0][1] * float(i - center_y))
            if new_x + center_x >= _img.shape[1] or new_x + center_x < 0:
                continue
            new_y = int(rotation_matrix[1][0] * float(j - center_x) + rotation_matrix[1][1] * float(i - center_y))
            if new_y + center_y >= _img.shape[0] or new_y + center_y < 0:
                continue
            _img[i, j] = img[new_y + center_y, new_x + center_x]
    return _img


def align(img, is_object=lambda x: True if x == 0 else False, background=255):
    min_height = 0
    _img = np.ones(img.shape, dtype=np.uint8) * background
    height = img.shape[0] - 1
    for j in range(img.shape[1]):
        first_on_col = True
        for i in reversed(range(img.shape[0])):
            if is_object(img[i, j]):
                if first_on_col:
                    min_height = height - i
                    first_on_col = False
                    _img[height, j] = 0
                else:
                    _img[min_height + i, j] = 0
    return _img


def spaceless(projection):
    exited = True
    entered = False
    bounds = []
    enter_coord = 0
    for i in range(projection.shape[0]):
        if projection[i] > 0 and exited:
            exited = False
            entered = True
            enter_coord = i
        elif projection[i] == 0 and entered:
            exited = True
            entered = False
            bounds.append((enter_coord, i))
    if entered is True and exited is False:
        bounds.append((enter_coord, projection.shape[0] - 1))
    return bounds


def mean_filter(img):
    dest = np.copy(img)
    for i in range(1, img.shape[0] - 1):
        for j in range(1, img.shape[1] - 1):
            mean = 0
            for k, v in zip(neighbours[1][0], neighbours[1][1]):
                mean += img[i + k, k + v]
            mean += img[i, j]
            dest[i, j] = int(mean / 9.0)
    return dest


def hor(img):
    dest = np.copy(img)
    for i in range(img.shape[0]):
        for j in range(1, img.shape[1] - 1):
            if img[i, j-1] < img[i, j] > img[i, j + 1]:
                dest[i, j] = 255
    return dest


def get_connected_components(img, is_object=lambda x: True if x < 255 else False):
    labels = np.zeros_like(img, dtype=np.uint32)
    label = 0
    for i in range(1, img.shape[0] - 1):
        for j in range(1, img.shape[1] - 1):
            if is_object(img[i, j]) and labels[i, j] == 0:
                label_queue = queue.Queue(maxsize=img.shape[0] * img.shape[1])
                label += 1
                labels[i, j] = label
                label_queue.put((i, j))
                while not label_queue.empty():
                    coord = label_queue.get()
                    for nbh in zip(neighbours[1][0], neighbours[1][1]):
                        if 0 <= coord[0] + nbh[0] < img.shape[0] and 0 <= coord[1] + nbh[1] < img.shape[1]:
                            if is_object(img[coord[0] + nbh[0], coord[1] + nbh[1]]) and \
                                    labels[coord[0] + nbh[0], coord[1] + nbh[1]] == 0:
                                labels[coord[0] + nbh[0], coord[1] + nbh[1]] = label
                                label_queue.put((coord[0] + nbh[0], coord[1] + nbh[1]))
    return labels, label


def crop_connected_symbol(img, labels, label):
    conn_compt = {}
    conn_max = {}
    conn_min = {}
    for i in range(label):
        conn_compt[i + 1] = []
        conn_max[i + 1] = [0, 0]
        conn_min[i + 1] = [10000, 10000]

    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if labels[i, j] > 0:
                conn_compt[labels[i, j]].append([i, j])
                if j >= conn_max[labels[i, j]][1]:
                    conn_max[labels[i, j]][1] = j
                elif i >= conn_max[labels[i, j]][0]:
                    conn_max[labels[i, j]][0] = i
                if j <= conn_min[labels[i, j]][1]:
                    conn_min[labels[i, j]][1] = j
                elif i <= conn_min[labels[i, j]][0]:
                    conn_min[labels[i, j]][0] = i
    symbols = []
    for i in range(1, label + 1):
        symbol = np.ones((conn_max[i][0] - conn_min[i][0] + 5, conn_max[i][1] - conn_min[i][1] + 5), dtype=np.uint8) \
                 * 255
        for coord in conn_compt[i]:
            symbol[coord[0] - conn_min[i][0] + 2, coord[1] - conn_min[i][1] + 2] = img[coord[0], coord[1]]
        symbols.append((symbol, conn_min[i][1]))
        # cv2.imshow("symbol", symbol)
        # cv2.waitKey()
    symbols = sorted(symbols, key=lambda x: x[1])
    return [x[0] for x in symbols]
