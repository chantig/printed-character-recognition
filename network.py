import json
import random
import numpy as np


class QuadraticCost(object):

    @staticmethod
    def cost(a, y):
        return 0.5*np.linalg.norm(a-y)**2

    @staticmethod
    def cost_deriv(z, a, y):
        return (a-y) * sigmoid_prime(z)


class CrossEntropyCost(object):

    @staticmethod
    def cost(a, y):
        return np.sum(np.nan_to_num(-y*np.log(a)-(1-y)*np.log(1-a)))

    @staticmethod
    def cost_deriv(z, a, y):
        return a-y


class NeuralNetwork(object):

    const_init = {
        'q': QuadraticCost,
        'c': CrossEntropyCost
    }

    def __init__(self, sizes, cst):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(y, x) / np.sqrt(x)
                        for x, y in zip(self.sizes[:-1], self.sizes[1:])]
        self.cost = self.const_init[cst]

    def feed_forward(self, a):
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w, a)+b)
        return a

    def sgd(self, training_data, epochs, batch_size, learning_rate, lamda=0.1, test_data=None):
        n_test = 0
        if test_data:
            n_test = len(test_data)
        n = len(training_data)
        for j in xrange(epochs):
            random.shuffle(training_data)
            batches = [training_data[k:k + batch_size] for k in xrange(0, n, batch_size)]
            for mini_batch in batches:
                self.update_mini_batch(mini_batch, learning_rate, lamda, n)
            if test_data:
                e = self.evaluate(test_data)
                print "Epoch {0}: {1} / {2}, percentage = {3}".format(
                    j, e, n_test, round(float(e) / float(n_test), 3))
            else:
                print "Epoch {0} complete".format(j)

    def update_mini_batch(self, batch, learning_rate, lamda, n):
        new_b = [np.zeros(b.shape) for b in self.biases]
        new_w = [np.zeros(w.shape) for w in self.weights]
        for x, y in batch:
            delta_d, delta_w = self.back_propagation(x, y)
            new_b = [nb+dnb for nb, dnb in zip(new_b, delta_d)]
            new_w = [nw+dnw for nw, dnw in zip(new_w, delta_w)]
        self.weights = [(1-learning_rate*(lamda/n)) * w - (learning_rate / len(batch)) * nw
                        for w, nw in zip(self.weights, new_w)]
        self.biases = [b - (learning_rate / len(batch)) * nb
                       for b, nb in zip(self.biases, new_b)]

    def back_propagation(self, x, y):
        new_b = [np.zeros(b.shape) for b in self.biases]
        new_w = [np.zeros(w.shape) for w in self.weights]
        activation = x
        activations = [x]
        zs = []
        for b, w in zip(self.biases[:-1], self.weights[:-1]):
            z = np.dot(w, activation)+b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)
        z = np.dot(self.weights[-1], activation) + self.biases[-1]
        zs.append(z)
        activations.append(softmax(z))
        delta = self.cost.cost_deriv(zs[-1], activations[-1], y)
        new_b[-1] = delta
        new_w[-1] = np.dot(delta, activations[-2].transpose())
        for l in xrange(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            new_b[-l] = delta
            new_w[-l] = np.dot(delta, activations[-l-1].transpose())
        return new_b, new_w

    def evaluate(self, test_data):
        test_results = [(np.argmax(self.feed_forward(x)), np.argmax(y))
                        for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)

    def save(self, filename):
        if filename is None or filename == '':
            filename = 'network'
        data = {"sizes": self.sizes,
                "weights": [w.tolist() for w in self.weights],
                "biases": [b.tolist() for b in self.biases],
                "cost": str(self.cost.__name__).lower()[0]}
        f = open(filename + '.json', "w")
        json.dump(data, f)
        f.close()


def load(filename):
    f = open(filename, "r")
    data_j = json.load(f)
    f.close()
    cst = data_j["cost"]
    net = NeuralNetwork(data_j["sizes"], cst=cst)
    net.weights = [np.array(w) for w in data_j["weights"]]
    net.biases = [np.array(b) for b in data_j["biases"]]
    return net


def sigmoid(z):
    return 1.0/(1.0+np.exp(-z))


def sigmoid_prime(z):
    return sigmoid(z)*(1-sigmoid(z))


def cost_derivative(output_activations, y):
    return output_activations - y


def softmax(zs):
        activations = np.exp(zs)
        total = np.sum(activations)
        return activations / total
