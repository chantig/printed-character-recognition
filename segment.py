import cv2
import numpy as np
import queue

from preprocessing import otsu_threshold, binarization, valleys_peaks, \
    project, histogram_smoothing, histogram, extract_heights, find_skew, \
    get_geo, rotate_by_angle, spaceless, squeeze, extract_mins, display_histogram, get_connected_components, \
    crop_connected_symbol, erode, neighbours


class WordSegmentator:
    def __init__(self, imag, is_object=None):
        self.imag = imag
        self.is_object = is_object

    def get_words(self):
        y_project, _ = project(self.imag)
        lines = self.find_lines(y_project)
        # cv2.imshow("img", self.imag)
        for line in lines:
            _area, center_x, center_y = get_geo(self.imag[line[0]: line[1], :])
            angle = find_skew(self.imag[line[0]: line[1], :])
            _img = rotate_by_angle(self.imag[line[0]: line[1], :], angle, center_x, center_y)
            cols = self.find_words(project(_img)[1])
            for col in cols:
                # pr_y, pr_x = project(_img[:, col[0]: col[1]])
                # line0, line1 = squeeze(pr_y)
                yield self.imag[line[0]: line[1], col[0]: col[1]]

    def find_lines(self, projection):
        """Should return a (start_row, end_row)"""
        raise NotImplementedError("implement this method in subclasses")

    def find_words(self, projection):
        """Should return a (start_col, end_col)"""
        raise NotImplementedError("implement this method in subclasses")


class LocalMinimaWordSegmentator(WordSegmentator):

    def __init__(self, imag):
        WordSegmentator.__init__(self, imag)

    def find_lines(self, projection):
        smoothed_histo = histogram_smoothing(projection, kernel_size=11)
        cv2.imshow("line", display_histogram(smoothed_histo))
        # cv2.waitKey()
        valleys, _ = valleys_peaks(smoothed_histo, 1)
        lines = []
        for i in range(len(valleys) - 1):
            lines.append((valleys[i], valleys[i + 1]))
        return lines

    def find_words(self, projection):
        smoothed_histo = histogram_smoothing(projection, kernel_size=5)
        cv2.imshow("asdasd", display_histogram(smoothed_histo))
        # cv2.waitKey()
        return spaceless(smoothed_histo)


class SpaceLineSegmentator(WordSegmentator):

    def find_words(self, projection):
        smoothed_histo = histogram_smoothing(projection, kernel_size=5)
        # cv2.imshow("asdasd", display_histogram(smoothed_histo))
        # cv2.waitKey()
        return spaceless(smoothed_histo)

    def find_lines(self, projection):
        smoothed_histo = histogram_smoothing(projection, kernel_size=5)
        # cv2.imshow("asdasd", display_histogram(smoothed_histo))
        # cv2.waitKey()
        return spaceless(smoothed_histo)


class CharacterSegmentator:
    def __init__(self, imag, dims):
        self.dims = dims
        self.segmentator = LocalMinimaWordSegmentator(imag)

    def get_segmented_word(self):
        for _word in self.segmentator.get_words():
            cv2.imshow("boer", _word)
            proj_y, proj_x = project(_word)
            cv2.imshow("proj_y", display_histogram(proj_y, _word.shape[0]))
            cv2.imshow("proj_x", display_histogram(proj_x, _word.shape[0]))
            # _word = erode(_word, )
            # cv2.imshow("oer", _word)
            # cv2.waitKey()
            chars = []
            for _char in self.get_chars(_word):
                # cv2.waitKey()
                # thrs = otsu_threshold(histogram(_char))
                # _char = binarization(_char, thrs)
                cv2.imshow('in seg', _char)
                cv2.waitKey()
                # _char = np.reshape(_char, (self.dims[0] * self.dims[1], 1))

                chars.append(_char)
            yield chars

    def get_chars(self, _word):
        raise NotImplementedError("implement this method in subclasses")


class PrintedCharacterSegmentator(CharacterSegmentator):

    def get_chars(self, _word):
        spaces = spaceless(project(_word)[1])
        for space in spaces:
            yield _word[:, space[0]: space[1]]

    def __init__(self, imag, dims):
        CharacterSegmentator.__init__(self, imag, dims)
        self.dims = dims


class PrintedConnectedCharacterSegmentator(CharacterSegmentator):

    def get_chars(self, _word):
        labels, label = PrintedConnectedCharacterSegmentator.char_conn_component(_word)
        return crop_connected_symbol(_word, labels, label)

    def __init__(self, imag, dims):
        CharacterSegmentator.__init__(self, imag, dims)
        self.dims = dims

    @staticmethod
    def char_conn_component(_word, is_object=lambda x: True if x < 255 else False):
        labels = np.zeros_like(_word, dtype=np.uint32)
        label = 0
        for i in range(1, _word.shape[0] - 1):
            for j in range(1, _word.shape[1] - 1):
                if is_object(_word[i, j]) and labels[i, j] == 0:
                    label_queue = queue.Queue(maxsize=_word.shape[0] * _word.shape[1])
                    label += 1
                    labels[i, j] = label

                    label_queue.put((i, j))
                    for v in range(i, _word.shape[0] - 1):
                        if is_object(_word[v, j]):
                            labels[v, j] = label
                            label_queue.put((v, j))
                    while not label_queue.empty():
                        coord = label_queue.get()
                        for nbh in zip(neighbours[1][0], neighbours[1][1]):
                            if 0 <= coord[0] + nbh[0] < _word.shape[0] and 0 <= coord[1] + nbh[1] < _word.shape[1]:
                                if is_object(_word[coord[0] + nbh[0], coord[1] + nbh[1]]) and \
                                        labels[coord[0] + nbh[0], coord[1] + nbh[1]] == 0:
                                    labels[coord[0] + nbh[0], coord[1] + nbh[1]] = label
                                    label_queue.put((coord[0] + nbh[0], coord[1] + nbh[1]))
        return labels, label


if __name__ == "__main__":
    img = cv2.imread("./datas/transform/text.png", cv2.IMREAD_GRAYSCALE)

    histo = histogram(img)
    thresh = otsu_threshold(histo)
    im_bw = binarization(img, thresh)

    cv2.imshow("ed", im_bw)
    cv2.waitKey()
    asd = PrintedConnectedCharacterSegmentator(im_bw, (28, 28))
    for word in asd.get_segmented_word():
        print "word"


class HandwrittenCharacterSegmentator(CharacterSegmentator):

    def get_chars(self, _word):
        pass

    def __init__(self, imag, dims):
        CharacterSegmentator.__init__(self, imag, dims)

    def get_segmented_word(self):
        ws = []
        for w in self.segmentator.get_words():
            heights = extract_heights(w)
            mins = extract_mins(w)
            start, end = squeeze(heights)
            hei = histogram_smoothing(heights[start: end])
            hh = int(np.mean(mins))
            bb = int(np.mean(hei))
            valleys, peaks = valleys_peaks(hei, 1)
            accepted_valleys = [start]
            for v in valleys:
                if (hh <= hei[v] < bb + 2 and start + v != start) or hei.shape[0] - 1 == v:
                    accepted_valleys.append(start + v)
            if len(valleys) == len(peaks):
                accepted_valleys.append(end)
            chars = []
            cv2.imshow("w", w)
            for i in range(len(accepted_valleys) - 1):
                chars.append(w[:, accepted_valleys[i]: accepted_valleys[i + 1]])
                cv2.imshow("char", w[:, accepted_valleys[i]: accepted_valleys[i + 1]])
                cv2.waitKey()
            ws.append(chars)
        return ws
