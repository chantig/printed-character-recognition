# Recunoasterea Textului Printat
## Introucere
###Descrierea Temei
Tema acestui proiect este de a transforma textul din imagini in text asupra caruia 
se pot aplica tehnici de prelucrare(cautarea de cuvinte, copiere, lupire, etc). Pentru 
a ajunge la rezultatul dorit am folosit tehnici de procesare de imagini si de machine learning.
### Motivatia
Operatiile(de prelucrare a textelor) existente ofera utilizatorilor posibilitatea de a cauta, edita si partaja
informatia intr-o maniera simpla si rapida. Textul care se afla sub forma de imagine nu poate sa fie supus unor astfel de 
operatii. Astfel, am incercat sa gasesc o rezolvare pentru aceasta problema.
## Studiu Bibliografic
### Handwritten Text Recognition using Deep Learning 
In acest articol sunt descrise doua metode de clasificare a cuvintelor. Prima se foloseste
de Retele Convolutionale Profunde pentru a scana si a clasifica cuvintele. Astfel rezolvand problema segmentarii.
A doua tehnica propusa se foloseste de LSTM pentru a segmenta cuvintele. Dupa segmentare, fiecare cuvant va trece
printr-o retea convolutionala pentru a fi clasificat. 
### Pattern Matching
Aceasta metoda se foloseste de suprapunerea caracterelor model peste caracterele din text, 
Astfel, caracterul este clasificat in functie de eroarea obtinuta.
### Two step recognition
Dupa cum specifica si numele, metoda asta presupune doi pasi: Primul pas fiind segmentarea, iar
al doilea pas fiind clasificarea. In functie de tipul de text(scris de mana sau scris de tipar) se 
folosesc diferite metode de segmentare. Pentru partea de clasificare se pot folosi mai multe tehnici, cu ar fi:
SVM, CNN, Retele Profunde.
    
## Solutia Propusa
### Schema bloc
![Schema bloc al aplicatiei](readme_images/srf_flow.png)
### Descrierea Pasilor
#### Preprocesare
Dupa cum apare in diagrama, preprocesarea presupune doua operatii. In cazul imaginilor de dimensiuni
mari timpul de aplicare a unor filtre este ridicat. Astfel, cu ajutorul scalarii, imaginea este
adusa la o dimensiune mai mica. 
Dupa scalare, se aplica o tehnica de binarizare pentru a separa pixelii background de pixelii obiect.
#### Segmentarea Cuvintelor
Inainte de a segmenta cuvintele trebuie rezolvata problema segmentarii liniilor. 
Pentru gasirea liniilor se va face o proiectare verticala, iar pe baza histogramei rezultate 
se vor gasi liniile. Aceasta tehnica are acuratete mare in cazul in care panta liniilor este destul de mica. 
Pentru fiecare linie gasita se va face proiectie pe orizontala. Pentru textele printate exista un spatiu intre fiecare cuvant.
Pe baza acestor saptii se vor segmenta cuvintele.
#### Segmentarea Caracterelor
In schema bloc, segmentarea a fost impartita in doua deoarece segmetarea caracterelor este
o problema grea, neexistand o tehnica care sa aiba acuratete ridicata pentru fiecare caracter.
In cazul acestei probleme exista mai multe tehnici: 

* Segmetarea pe baza spatiilor existente intre caractere. In cazul(cazul ideal) textului printat, caracterele trebuie sa fie 
    despartite intre ele de un mic spatiu. Chiar daca exista un spatiu intre caractere, in cazul unor caractere proiectia orizontala nu evidentiaza spatiile necesare.
* Segmentarea pe baza histogramei de inaltime. Cu ajutorul acestei histograme se pot extrage anumite puncte minime
    cu ajutorul carora se poate aproxima limitele dintre caractere. Ca si in cazul anterior, apar anumite exceptii, cum ar fi:
* Caracterele sunt conectate la inaltime ridicata, caz in care punctul nu este o "vale".
* Literele "u", "v", "w", "y" presupune cel putin o vale, astfel acestea fiind segmentate gresit.
* Segmentare pe baza componetelor conectate. Aceasta tehnica poate avea doua exceptii:
    * Multiple litere conecate fiind segmentate ca si una
    * Literele "i" si "j" fiind segmentate in parti. Aceasta problema fiind rezolvata cu ajutorul urmataorei tehnici:
        * Se scaneaza imaginea de la dreapta spre stanga, de sus pana jos. In momentul in care am gasit primul pixel obiect,
            toti pixeli obiect de pe coloana vor avea eticate primului pixel gasit.

#### Clasificarea Caracterelor
Dupa ce caracterele au fost segmentate, acestea vor trece printr-un calsificator. Acest clasificator va
unul de tipul Retelelor Neuronale. Fiecare caracter fiind scalat astfel incat sa se potriveasca cu numarul neuronilor de la intrare.
    
## Detalii de Implementare 
Implementarea acestui proiect a fost facuta in python 2.7. Librariile folosite sunt opencv si numpy.
In urmatoarele paragrafe voi incerca sa explic modul de implementare a pasilor care apara in "flow diagram".
### Implementarea Pasului de Preprocesare
* Pentru a face scalarea imagini am folosit functiile dinsponibile in libraria opencv
* Partea de binarizare se imparte in doua subprobleme:
    * Gasirea dinamica a unui prag de binarizare. Pentru gasirea acestui prag s-a folosit metoda [otsu clasica](https://en.wikipedia.org/wiki/Otsu's_method). Aceasta presupune ca variatia inter-clasa sa fie maxima. 
    * Dupa gasirea pragului, toti pixelii cu valori mai mici de prag vor fi marcati ca si pixeli obiect, iar restul pixelilor vor fi marcati ca si pixeli de background.
### Implementarea Paslui de Segmentare a Cuvintelor
* La fel ca si in cazul binarizarii, pasul acesta se va imparti in doua subprobleme:
    * Gasirea fiecarei linii. Aceasta se va face folosind proiectia pe axa OY. Histograma rezultata in urma proiectiei va fi netezita.
        scapand zgomote. O vale histograma implica un spatiu intre randuri. Pentru a scoate vaile histograme am folosit algoritmul [peaks and valleys](http://billauer.co.il/peakdet.html).
        * Pentru fiecare linie gasita se va face o proiectie pe axa OX, se va aplica o netezire histograme. Pe histograma netezista
            se vor cauta spatiile libere. Aceste spatii fin spatiile dinte cuvinte.
### Implementarea Psului de Segmentare a Characterelor
* Caracterele vor fi segmentate dupa metoda [componentelor conectate](https://en.wikipedia.org/wiki/Connected-component_labeling). Aceasta tehnica are 
   mai multe abordari. Pentru implementare am folosit o versiune modifica a algoritmului [BFS](https://en.wikipedia.org/wiki/Breadth-first_search). Modificarea
   consta in: Pentru fiecare prima gasire a unui pixel obiect, toti pixelii obiect de pe coloana vor fi pusi in coada pentru a fi procesati.         
### Implemetarea clasificatorului
* Clasificatorul este unul de tipul Retelelor Neuronale. Reteaua a fost implementata sub forma de matrici. Pe langa ca complexitatea implemetarii
  este scazuta, CPU-rile si GPU-rile suporta operatii pe numere multidimesionale. In continoare voi descrie implementarea fiecarei parti a
   clasificatorului:
    * FeedForward: A fost implemetat folosind inmultirile de matrici.
    * Weights: Pentru initializare s-a folosit o metoda de normalizare astfel incat o mica modificare in acesti parametri sa poata sa modifice 
        Costul.
    * Activation Function: Acestea au fost de tipul Sigmoid, iar pentru ultimul layer sa folosit Softmax.
    * BackPropagation: Implementarea acestui algoritm se imparte in mai multe parti:
        * Prima parte consta dintr-un feedforward.
        * Se afla eroare de la ultimul nivel.
        * In ordinea inversa se va propaga eroarea si se vor modifica parametri.
        * Ecuatiile necesare pentru acest algoritm:
![BP Equations](readme_images/ecc_bp.png)       
    * Stochastic Gradient Descent: Acest algoritm presupune spargerea datelor de train in mai multe 
    batch-uri. Pentru fiecare batch se vor calcula delta_weights. Pentru fiecare batch, parametrii retelei se vor 
    schimba in functie de delta_weights. Aceasta se va repeta pentru un anumit numar de iteratii.
    * Pentru o mai buna performanta am folosit regularizare L2. Aceasta este benefica deoarece ajuta clasificator sa nu
    ramana blocat intr-un maxim local. 
    * Pentru o convergenta mai buna, in loc de Quadratic Cost, am folost Cross Entropy Cost. Aceasta ajuta la convergenta
    deoarece derivata acestei functii depinde de diferenta dintre "target label" si "predicted label" 
        
## Rezultate experimentale
Aplicatia fiind impartita in doua module, segmentare si clasificare, testarea a fost facuta pentru fiecare parte. In urmatoarele paragrafe o sa
evidentie rezultatele testarilor:
### Testarea clasificatorului 
Setul de data pentru antrenare consta din caractere scrise(cu cate un caracter "space" intre ele) intr-un editor in ordinea alfabetica cu diferite fonturi.
Pentru fiecare document scris cu aceste litere a fost facuta cate o poza. Stiindu-se ordinea caracterelor, treaba ramasa este de a extrage caracterele si a le salva individual
cu eticheta corespunzatoare. Eticheta a fost salvata in numele imaginilor. Numarul de caractere extrase a fost aproximativ 21.000.
In momentul antrenarii, fiecare caracter a fost incarcat, iar fiecare eticheta a fost transformata in "one hot encoding". Inainte de a incepe antrenare,
datele au fost amestecate. In urmatoarele paragrafe vor fi prezentate antrenarile anumitor retele: 
#### Retea Neuronala 1
* layers: 4 
* neuroni / layer: [784 110 100 26] 
* learning rate: 0.004
* lambda: 0.2
* Cost Function: CrossEntropy
* Activation: Sigmoid
* Last Layer: Softmax

![Antrenare 1](readme_images/learning/learning1.png)
#### Retea Neuronala 2
* layers: 3 
* neuroni / layer: [784 100 26] 
* learning rate: 0.001
* lambda: 0.2
* Cost Function: CrossEntropy
* Activation: Sigmoid
* Last Layer: Softmax

![Antrenare 2](readme_images/learning/learning2.png)
### Testarea segmentatorului
Pentru testarea segmentatorului s-au folosit mai multe texte scrise, asupra carora a fost aplicat. 

## Bibliografie
1. [Explicatie Retele neuronale](http://neuralnetworksanddeeplearning.com/ )
1. [Explicatie Otsu](https://en.wikipedia.org/wiki/Otsu's_method)
1. [Explicatie Parte Matematica Retele Neuronale(YouTube)](https://www.youtube.com/watch?v=aircAruvnKk&list=PLLMP7TazTxHrgVk7w1EKpLBIDoC50QrPS)
1. [Printed Character Segmentation 1](https://pdfs.semanticscholar.org/50ea/7de7145857e91eed0e74e6692bd24ff1c5cf.pdf)
1. [Printed Character Segmentation 2](https://globaljournals.org/GJCST_Volume13/3-Character-Segmentation-for-Telugu.pdf)
1. [Printed Character Segmentation 3](https://papers.nips.cc/paper/461-recognizing-overlapping-hand-printed-characters-by-centered-object-integrated-segmentation-and-recognition.pdf)
1. [Printed Character Segmentation 4](http://blog.mathocr.com/2017/06/09/handwritten-character-segmentation.html)
1. [HandWritten Character Segmentation 1](https://ac.els-cdn.com/S1877050913001464/1-s2.0-S1877050913001464-main.pdf?_tid=7a033a71-db2a-48c0-babf-20aa76f12d92&acdnat=1547643903_94a223450a8182eef901b4f5afb20f96)
