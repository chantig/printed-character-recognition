import os
import random

import cv2
import numpy as np

from preprocessing import otsu_threshold, binarization, histogram, project, squeeze
from segment import SpaceLineSegmentator

data = {
    0: 'a', 1: 'b', 2: 'c', 3: 'd', 4: 'e',
    5: 'f', 6: 'g', 7: 'h', 8: 'i', 9: 'j',
    10: 'k', 11: 'l', 12: 'm', 13: 'n', 14: 'o',
    15: 'p', 16: 'q', 17: 'r', 18: 's', 19: 't',
    20: 'u', 21: 'v', 22: 'w', 23: 'x', 24: 'y',
    25: 'z'
}
data_inv = dict((y, x) for x, y in data.iteritems())


def get_data(imag):
    img = cv2.imread(imag, cv2.IMREAD_GRAYSCALE)
    histo = histogram(img)
    thresh = otsu_threshold(histo)
    img = binarization(img, thresh)
    segmentator = SpaceLineSegmentator(img)
    for word in segmentator.get_words():
        yield word


def create_data(images, to='./datas/data/'):
    i = 0
    j = 0
    for image in images:
        for d in get_data(image):
            print data[i]
            cv2.imwrite(to + data[i] + str(j) + '.png', cv2.resize(d, (28, 28)))
            i = (i + 1) % 26
            j += 1


def get_names(folder='./datas/train/'):
    return [folder + str(x) for x in os.listdir(folder) if x.endswith('.png')]


def load_data(folder='./datas/data/', pr=0.8):
    features = []
    labels = []
    i = 0
    for image in os.listdir(folder):
        im = cv2.imread(folder + str(image), cv2.IMREAD_GRAYSCALE)
        # im = binarization(im, 2)
        pr_y, pr_x = project(im)
        st_y, end_y = squeeze(pr_y)
        st_x, end_x = squeeze(pr_x)
        im = im[st_y: end_y, st_x:end_x]
        im = cv2.resize(im, (28, 28))
        im = np.reshape(im, (784, 1))
        features.append(im)
        labels.append(to_one_hot(data_inv[image[0]]))
        print i
        i += 1
    tr_size = int(pr * len(features))
    training_data = zip(features, labels)
    random.shuffle(training_data)
    return training_data[:tr_size], training_data[tr_size:]


def to_one_hot(j):
    e = np.zeros((26, 1))
    e[j] = 1.0
    return e


def get_images(no, folder='./datas/data/'):
    features = []
    labels = []
    i = 0
    for image in os.listdir(folder):
        im = cv2.imread(folder + str(image), cv2.IMREAD_GRAYSCALE)
        # im = binarization(im, 2)
        pr_y, pr_x = project(im)
        st_y, end_y = squeeze(pr_y)
        st_x, end_x = squeeze(pr_x)
        im = im[st_y: end_y, st_x:end_x]
        im = cv2.resize(im, (28, 28))
        im = np.reshape(im, (784, 1))
        features.append(im)
        labels.append(image[0])
        if i == no:
            break
        i += 1
    return zip(features, labels)


if __name__ == '__main__':
    create_data(get_names())
