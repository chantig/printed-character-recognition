import cv2

from network import NeuralNetwork
import numpy as np
from data_loader import load_data, get_images, data
import sys


def to_one_hot(label):
    c = np.zeros((10, 1))
    c[label] = 1.0
    return c


def data_loader(img, labels):
    _feature = [np.reshape(x, (784, 1)) for x in img]
    _label = [to_one_hot(x) for x in labels]
    return zip(_feature, _label)


if __name__ == "__main__":
    pr = float(sys.argv[5])
    training_data, test_data = load_data(pr=pr)
    layers = int(sys.argv[1])
    network = NeuralNetwork([784, 100, layers, 26], sys.argv[6])
    batches = int(sys.argv[2])
    leaning = float(sys.argv[3])
    lamda = float(sys.argv[4])
    iterations = int(sys.argv[7])
    network.sgd(training_data, iterations, batches, leaning, lamda, test_data=test_data)
    for image in get_images(40):
        lab = data[np.argmax(network.feed_forward(image[0]))]
        print "actual label: ", image[1], " predicted label: ", lab
        cv2.imshow("imaggg",np.reshape(image[0], (28, 28)))
        cv2.waitKey()
    print "save the net?[y/n]"
    save_filename = str(raw_input())
    while save_filename != 'y' and save_filename != 'n':
        print "input [y/n]"
        save_filename = str(raw_input())
    if save_filename == 'y':
        filename = str(raw_input("give me the filename!!"))
        network.save(filename=filename)
